
const express = require("express");
const auth = require("../auth");

// Creates a router instance that functions as a middleware and routing system
const router = express.Router();

// Route for checking if the user's email already exists in the database
const userController = require("../controllers/userController");

// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details
router.post("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);

// Provides the user's ID for the getProfile controller method
userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});


// [Section] S41
// Route for authenticated user enrollment
router.post("/enroll", auth.verify, (req, res) => {
		let data = {
			userId: auth.decode(req.headers.authorization).id,
			courseId: req.body.courseId
		}
		userController.enroll(data).then(resultFromController => res.send(resultFromController));
});








/////////
router.get("/view", (req, res) => {
	userController.getView().then(resultFromController => res.send(resultFromController));
});


router.get("/:id", (req, res) => {
	userController.getTaskById(req.params.id).then(resultFromController => res.send(resultFromController));
});



// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;




